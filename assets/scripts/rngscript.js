let player = {
	hp: 89,
	ac: 21
}
let orc = {
	hp: 93,
	ac: 16
}


let playerhp = document.getElementById('plhp');
let enemyhp = document.getElementById('enhp');
let playerAtkRoll = document.getElementById('playerAtkRoll');
let playerSuccess = document.getElementById('playerSuccess');
let playerDmg = document.getElementById('playerDmg');
let enemyAtkRoll = document.getElementById('enemyAtkRoll')
let enemySuccess = document.getElementById('enemySuccess');
let enemyDmg = document.getElementById('enemyDmg');

playerhp.textContent = player.hp;
enemyhp.textContent = orc.hp;

let playerTurn = true;
let enemyTurn = false;

const playerAtk = () => {
	//atk roll
	let d20 = Math.floor((Math.random() * 20) + 1);
	let d8x = Math.floor((Math.random() * 8) + 1);
	let d8y = Math.floor((Math.random() * 8) + 1);
	let d8z = Math.floor((Math.random() * 8) + 1);
	let atk = false;

	if ((d20+8)>=orc.ac) {
		atk = true;
	}

	if (atk === true) {
		playerSuccess.textContent = "Success hit!"
		playerDmg.textContent = "You dealt ("+d8x+")"+"("+d8y+")"+"("+d8z+")"+"="+(d8x+d8y+d8z)+" damage";
		orc.hp -= (d8x+d8y+d8z);
		enemyhp.textContent = orc.hp;
	} else {
		playerSuccess.textContent = "You Missed!"
		playerDmg.textContent = "0 dmg"
	}

	playerAtkRoll.textContent = "1d20+8("+d20+"+8 ="+(d20+8)+")";
	
}


const enemyAtk = () => {
	//atk roll
	let d20 = Math.floor((Math.random() * 20) + 1);
	let d12x = Math.floor((Math.random() * 12) + 1);
	let d8y = Math.floor((Math.random() * 8) + 1);
	let atk = false;

	if ((d20+6)>=player.ac) {
		atk = true;
	}

	if (atk === true) {
		enemySuccess.textContent = "Success hit!"
		enemyDmg.textContent = "You dealt ("+d12x+")+("+d8y+")+6 ="+((d12x+d8y)+6);" damage";
		player.hp -= ((d12x+d8y)+6);
		playerhp.textContent = player.hp;
	} else {
		enemySuccess.textContent = "You Missed!"
		enemyDmg.textContent = "0 dmg"
	}

	enemyAtkRoll.textContent = "1d20+8("+d20+"+6 ="+(d20+6)+")";
	
}


// rng 1-20
// Math.floor((Math.random() * 20) + 1);

// atk roll orc
// document.getElementById("#").innerHTML = "1d20+6("+x+"+6 ="+(x+6)+")";

// atk roll player
// document.getElementById("#").innerHTML = "1d20+8("+x+"+8 ="+(x+8)+")";

// player dmg roll
// document.getElementById("#").innerHTML = "("+x+")"+"("+y+")"+"("+z+")"+"="+(x+y+z);

// orc dmg roll
// document.getElementById("#").innerHTML = "("+x+")+("+y+")+6 ="+((x+y)+6);
